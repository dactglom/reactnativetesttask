const initialState = {
    uploadedData: [],
    loading: false,
    errorMessage: ''
}

const GET_PICTURES_START = 'GET_PICTURES_START';
const CLEAR_DATA = 'CLEAR_DATA';
const GET_PICTURES_FULFILLED = 'GET_PICTURES_FULFILLED';
const GET_PICTURES_REJECTED = 'GET_PICTURES_REJECTED';

const picturesReducer = (state = initialState, action) => {
    switch(action.type){
        case GET_PICTURES_START:
            return {
                ...state,
                loading: true
            };
        case CLEAR_DATA:
            return {
                ...state,
                uploadedData: []
            }
        case GET_PICTURES_FULFILLED:
            return {
                ...state,
                uploadedData: [...state.uploadedData, action.data],
                loading: false
            }
        case GET_PICTURES_REJECTED:
            return {
                ...state,
                loading: false,
                errorMessage: action.error
            }
        default:
            return state    
    }
}

export default picturesReducer;