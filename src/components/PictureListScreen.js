import React from "react";
import { View, Button, ActivityIndicator, FlatList, StyleSheet } from "react-native";
import { connect } from 'react-redux';
import PictureNameTextInput from './PictureNameTextInput';
import { picturesFetch } from '../actions/picturesActions';
import PictureListItem from './PictureListItem';

class PictureListScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            currentPage: 1,
            text: ''
        }
    }

    componentDidMount(){
        this.props.getData();
    }

    dropPageToDefault = () => {
        this.setState({ currentPage: 1 })
    }

    setText = (text) => {
        this.setState({ text: text }, () => console.log(this.state.text + ' HERE'));
    }

    downloadMorePictures = () => {
        this.setState({
            currentPage: ++this.state.currentPage
        }, () => {
            this.props.getData(this.state.currentPage, this.state.text);
        })
    }

    render() {
        const { data, loading } = this.props;
        return (
            <View style={ styles.pictureListScreenWrapper }>
                <PictureNameTextInput
                    dropPage = {() => this.dropPageToDefault()}
                    page = {this.state.currentPage}
                    setText = {this.setText}
                />
                <View style = { styles.listWrapper }>
                    {
                        loading ? 
                            <ActivityIndicator size = 'large'/> :
                                <FlatList
                                    data = {data}
                                    renderItem = {({item}) =>
                                        <PictureListItem
                                            imageUrl = {item.urls.small}
                                            username = {item.user.username}
                                            description = {item.description}
                                            imageUri = {item.urls.regular}
                                        />
                                    }
                                    keyExtractor = {(item, id) => id.toString()}
                                    style = { styles.list }
                                />
                    }
                </View>
                <Button
                    title = "SHOW MORE PHOTOS"
                    onPress = {() => this.downloadMorePictures()}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    pictureListScreenWrapper: {
        flex: 1, alignItems: "center", 
        justifyContent: "space-around"
    },
    listWrapper: {
        flex: 1
    },
    list: {
        marginBottom: 20
    }
})

function mapStateToProps(state){
    return {
        data: state.picturesReducer.uploadedData,
        loading: state.picturesReducer.loading
    }
}

function mapDispatchToProps(dispatch){
    return {
        getData: (page, text) => {
            dispatch(picturesFetch(page, text))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PictureListScreen);