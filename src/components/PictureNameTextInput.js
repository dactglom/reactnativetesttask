import React from 'react';
import { StyleSheet, TextInput } from 'react-native';
import { connect } from 'react-redux';
import { picturesFetch } from '../actions/picturesActions';

const BLUE = '#428AF8';
const LIGHT_GRAY = '#D3D3D3';

class PictureNameTextInput extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isFocused: false,
            text: ''
        }
    }
    
    inputHandler = text => {
        this.setState({text: text}, () => {
            this.props.dropPage();
            this.props.setText(this.state.text);
            this.props.getData(this.props.page, this.state.text);
        });
    }

    handleFocus = e => {
        this.setState({ isFocused: true });
        if(this.props.onFocus){
            this.props.onFocus(e);
        }
    }

    handleBlur = e => {
        this.setState({ isFocused: false });
        if(this.props.onBlur){
            this.props.onBlur(e);
        }
    }
    
    render(){
        const { isFocused } = this.state;
        const { onFocus, onBlur, ...otherProps } = this.props;
        return (
            <TextInput
                onChangeText = {(text) => this.inputHandler(text)}
                value = {this.state.text}
                placeholder = "Search for photos..."
                selectionColor = {BLUE}
                underlineColorAndroid = {
                    isFocused ? BLUE : LIGHT_GRAY
                }
                onFocus = {this.handleFocus}
                onBlur = {this.handleBlur}
                style = {styles.textInput}
                {...otherProps}
            />
        )
    }
}

const styles = StyleSheet.create({
    textInput: {
        height: 40,
        paddingLeft: 6
    }
})

function mapDispatchToProps(dispatch){
    return {
        getData: (page, text) => {
            dispatch(picturesFetch(page, text))
        }
    }
}

export default connect(null, mapDispatchToProps)(PictureNameTextInput);