import React from "react";
import { View, Text, Image, Dimensions, TouchableOpacity, StyleSheet } from "react-native";
import { withNavigation } from 'react-navigation';

class PictureListItem extends React.Component{
    render(){
        return(
            <View
                style = { styles.pictureListItemWrapper }
            >
                <TouchableOpacity
                    onPress = {() => {
                        this.props.navigation.navigate('Picture', {
                            imageUri: this.props.imageUri
                        })
                    }}
                >
                    <Image
                        source = {{uri: this.props.imageUrl}}
                        style = { styles.listImage }
                    />
                </TouchableOpacity>
                <View style = { styles.userInfoWrapper }>
                    <Text
                        style = { styles.username }
                    >
                        {this.props.username}
                    </Text>
                    <Text style = { styles.imageDescription }>
                        {this.props.description ? this.props.description : 'No description provided...'}
                    </Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    pictureListItemWrapper: {
        flex: 1, 
        flexDirection: 'row', 
        justifyContent: 'space-around', 
        alignItems: 'center',
        marginTop: 20, 
        marginBottom: 20,
        width: Dimensions.get('window').width, 
        paddingLeft: 15, 
        paddingRight: 15
    },
    listImage: {
        width: 150, 
        height: 150, 
        borderRadius: 150/2
    },
    userInfoWrapper: {
        flex: 1, 
        marginRight: 10
    },
    username: {
        marginLeft: 40, 
        fontSize: 25, 
        textAlign: 'right', 
        flex: 1
    },
    imageDescription: {
        flex: 0.5,
        textAlign: 'right'
    }
})

export default withNavigation(PictureListItem);