import React from 'react';
import { View, Image, StyleSheet } from "react-native";

export default class PictureScreen extends React.Component{
    render(){
        const imageUri = this.props.navigation.getParam('imageUri');
        return(
            <View style = { styles.pictureWrapper }>
                <Image
                    source = {{uri: imageUri}}
                    style = { styles.picture }
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    pictureWrapper: {
        flex: 1
    },  
    picture: {
        flex: 1
    }
})