export function picturesFetch(page = 1, queryText = 'chicago'){
    return (dispatch) => {
        dispatch(getPicturesStart());
        fetch(`https://api.unsplash.com/search/photos?page=${page}&query=${queryText != '' ? queryText : 'chicago'}&client_id=9fd35ce34e207107ff2bcd9bfbec742960826138173d635fc328e929ab10c48f`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            mode: 'cors'
        }).then(data => data.json()).then(
            data => {
                console.log(data);
                console.log(page, queryText + ' HAPPIEST');
                if(page === 1){
                    dispatch(clearData());
                }
                data.results.forEach((item, i) => dispatch(getPicturesFulfilled(item)))
            }).catch(error => {
                dispatch(getPicturesRejected(error));
                console.log(error);
            })
    }
}

const getPicturesStart = () => {
    return { type: 'GET_PICTURES_START' }
}

const clearData = () => {
    return { type: 'CLEAR_DATA' }
}

const getPicturesFulfilled = (data) => {
    return { type: 'GET_PICTURES_FULFILLED', data: data }
}

const getPicturesRejected = (error) => {
    return { type: 'GET_PICTURES_REJECTED', error: error }
}