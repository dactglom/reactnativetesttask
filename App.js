import React, {Component} from 'react';
import { createAppContainer, createStackNavigator } from 'react-navigation';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import generalReducer from './src/reducers/index';
import PictureListScreen from './src/components/PictureListScreen';
import PictureScreen from './src/components/PictureScreen';

const store = createStore(
    generalReducer,
    composeWithDevTools(
      applyMiddleware(thunk)
    )
);

const AppNavigator = createStackNavigator({
    Home : PictureListScreen,
    Picture: PictureScreen
});

const AppContainer = createAppContainer(AppNavigator);

export default class App extends Component {
  render() {
    return (
      <Provider store = { store }>
        <AppContainer />
      </Provider>
    );
  }
}